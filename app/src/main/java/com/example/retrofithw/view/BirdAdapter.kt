package com.example.retrofithw.view

import android.view.LayoutInflater
import android.view.ViewGroup
import coil.load
import com.example.retrofithw.databinding.BirdItemBinding
import androidx.recyclerview.widget.RecyclerView


class BirdAdapter : RecyclerView.Adapter<BirdAdapter.BirdViewHolder>() {

    private var list: MutableList<String> = mutableListOf()

    fun updateList(newList: List<String>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class BirdViewHolder(val binding: BirdItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayIamge(image: String) {
            binding.ivBirdIamge.load(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BirdViewHolder {
        return BirdViewHolder(
            BirdItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BirdViewHolder, position: Int) {
        holder.displayIamge(list[position])
    }

    override fun getItemCount(): Int = list.size


}