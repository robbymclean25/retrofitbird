package com.example.retrofithw.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofithw.R
import com.example.retrofithw.databinding.FragmentMainBinding
import com.example.retrofithw.model.remote.BirdRepo
import com.example.retrofithw.viewModel.BirdVMFactory
import com.example.retrofithw.viewModel.BirdViewModel

class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding
    val factory = BirdVMFactory(BirdRepo())
    val viewModel: BirdViewModel by viewModels { factory }
    val adapter = BirdAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(LayoutInflater.from(context), container, false).also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvBirdie.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rvBirdie.adapter = adapter
        viewModel.birds.observe(viewLifecycleOwner) { birdState ->
            adapter.updateList(birdState.birds)
            binding.progress.isVisible = birdState.isLoading
        }
        viewModel.getBirds()
    }
}