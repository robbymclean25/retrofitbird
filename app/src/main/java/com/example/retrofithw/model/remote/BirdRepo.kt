package com.example.retrofithw.model.remote
import com.example.retrofithw.model.remote.BirdService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
class BirdRepo {

    private val birdService = BirdService.getInstance()

    suspend fun getBirds() = withContext(Dispatchers.IO){
        delay(1000)
        return@withContext birdService.getBirds(100)
    }
}