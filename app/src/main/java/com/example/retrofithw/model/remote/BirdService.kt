package com.example.retrofithw.model.remote
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.create
interface BirdService {

    companion object{

        private const val BASE_URL = "https://shibe.online"
        private const val ENDPOINT = "/api/birds"
        private const val CATS_ENDPOINT = "/api/cats"

        fun getInstance(): BirdService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(ENDPOINT)
    suspend fun getBirds(@Query("count") count : Int = 10) : List<String>
}