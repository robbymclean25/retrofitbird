package com.example.retrofithw.viewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofithw.model.remote.BirdRepo
import kotlinx.coroutines.launch
class BirdViewModel (val repo: BirdRepo) : ViewModel(){

    private val _birds: MutableLiveData<BirdState> = MutableLiveData(BirdState())

    val birds: LiveData<BirdState> get() = _birds

    fun getBirds(){
        viewModelScope.launch{
            _birds.value = BirdState(isLoading = true)
            val result = repo.getBirds()
            _birds.value = BirdState(birds = result, isLoading = false)
        }
    }

    data class BirdState(
        val isLoading : Boolean = false,
        val birds: List<String> = emptyList()
    )

}