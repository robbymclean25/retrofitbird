package com.example.retrofithw.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.retrofithw.model.remote.BirdRepo


class BirdVMFactory(
    private val repo: BirdRepo
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return BirdViewModel(repo) as T
    }
}
